#!/usr/bin/env bash
pip install -U pip
pip install -e .[dev]

kadi assets dev
kadi db init
kadi i18n compile

kadi run --host=0.0.0.0
