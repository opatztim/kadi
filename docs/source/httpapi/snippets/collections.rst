To get more detailed information about the contents of the requested/returned
representation of resources related to collections, see also :class:`.Collection` and
:class:`.Revision`. Note that for tags (:class:`.Tag`), only their names are relevant
for the API. For possible role values, see also the :ref:`Miscellaneous
<httpapi-latest-misc>` endpoints.
