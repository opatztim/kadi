Latest (v1)
============

This section documents the latest (in development) version of the HTTP API, currently
being version ``v1``.

.. toctree::
    :maxdepth: 1

    records
    collections
    templates
    users
    groups
    misc
