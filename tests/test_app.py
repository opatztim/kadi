# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import zlib

import pytest
from flask import current_app
from flask import json
from itsdangerous import base64_decode

import kadi.lib.constants as const
from kadi.lib.web import url_for
from tests.utils import check_api_response
from tests.utils import check_view_response


@pytest.mark.parametrize(
    "endpoint,method,status_code",
    [("/api/404", "get", 404), ("/api/records", "put", 405)],
)
def test_app_errorhandler_api_authenticated(
    endpoint, method, status_code, api_client, dummy_personal_token
):
    """Test if the app error handler works for authenticated API requests."""
    response = getattr(api_client(dummy_personal_token), method)(endpoint)
    check_api_response(response, status_code=status_code)
    # Check for any of the Flask-talisman headers.
    assert "Content-Security-Policy" in response.headers


@pytest.mark.parametrize(
    "endpoint,method", [("/api/404", "get"), ("/api/records", "put")]
)
def test_app_errorhandler_api_unauthenticated(endpoint, method, api_client):
    """Test if the app error handler works for unauthenticated API requests."""
    response = getattr(api_client("test"), method)(endpoint)
    check_api_response(response, status_code=401)
    # Check for any of the Flask-talisman headers.
    assert "Content-Security-Policy" in response.headers


@pytest.mark.parametrize(
    "endpoint,method,status_code",
    [("/404", "get", 404), ("/records/new", "put", 405)],
)
def test_app_errorhandler_views_authenticated(
    endpoint, method, status_code, client, user_session
):
    """Test if the app error handler works for authenticated non-API requests."""
    with user_session():
        response = getattr(client, method)(endpoint)

        check_view_response(response, status_code=status_code)
        # Check for any of the Flask-talisman headers.
        assert "Content-Security-Policy" in response.headers


@pytest.mark.parametrize(
    "endpoint,method",
    [("/404", "get"), ("/records/new", "put")],
)
def test_app_errorhandler_views(endpoint, method, client):
    """Test if the app error handler works for unauthenticated non-API requests."""
    response = getattr(client, method)(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == url_for("accounts.login")
    # Check for any of the Flask-talisman headers.
    assert "Content-Security-Policy" in response.headers


def _get_session_cookie(client):
    cookie = client.get_cookie(current_app.config["SESSION_COOKIE_NAME"])
    cookie_data = cookie.value
    compressed = False

    if cookie_data.startswith("."):
        cookie_data = cookie_data[1:]
        compressed = True

    cookie_data = base64_decode(cookie_data.split(".")[0])

    if compressed:
        cookie_data = zlib.decompress(cookie_data)

    return json.loads(cookie_data.decode())


def test_session_cookie(client, user_session):
    """Test if the session cookie is set correctly."""
    client.get("/records")
    session_cookie = _get_session_cookie(client)

    assert len(session_cookie) == 2

    # Flash messages from Flask and the redirect URL.
    for key in ["_flashes", const.SESSION_KEY_NEXT_URL]:
        assert key in session_cookie

    with user_session():
        client.get("/")
        session_cookie = _get_session_cookie(client)

        assert len(session_cookie) == 4

        # Flask-Login values and the CSRF token from Flask-WTF (due to the user session
        # fixture performing a POST request).
        for key in ["_id", "_fresh", "_user_id", "csrf_token"]:
            assert key in session_cookie

    session_cookie = _get_session_cookie(client)

    assert len(session_cookie) == 1
    # Only the CSRF token should remain.
    assert "csrf_token" in session_cookie
