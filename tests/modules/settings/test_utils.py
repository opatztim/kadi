# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.settings.utils import get_plugin_preferences_configs


def test_get_plugin_preferences_configs(user_session):
    """Test if plugin preferences configurations are retrieved correctly."""

    # We need to setup a user session, since the plugin hook currently does not pass
    # along a user, so the InfluxDB plugin relies on the current user being set.
    with user_session():
        configs = get_plugin_preferences_configs()

        assert len(configs) == 1
        assert "influxdb" in configs
