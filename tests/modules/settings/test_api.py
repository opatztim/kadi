# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.api.models import PersonalToken
from kadi.lib.oauth.models import OAuth2ServerToken
from kadi.lib.web import url_for
from tests.utils import check_api_response


@pytest.mark.parametrize(
    "endpoint",
    [
        "get_personal_tokens",
        "get_registered_applications",
        "get_authorized_applications",
    ],
)
def test_get_endpoints(endpoint, client, user_session):
    """Test the internal "api.get_*" endpoints related to settings."""
    with user_session():
        response = client.get(url_for(f"api.{endpoint}"))
        check_api_response(response)


def test_remove_personal_token(client, db, dummy_personal_token, user_session):
    """Test the internal "api.remove_personal_token" endpoint."""
    personal_token = PersonalToken.get_by_token(dummy_personal_token)

    with user_session():
        response = client.delete(
            url_for("api.remove_personal_token", id=personal_token.id)
        )
        db.session.commit()

        check_api_response(response, status_code=204)
        assert not PersonalToken.query.all()


def test_remove_oauth2_server_token(client, db, new_oauth2_server_token, user_session):
    """Test the internal "api.remove_oauth2_server_token" endpoint."""
    oauth2_server_token = new_oauth2_server_token()

    with user_session():
        response = client.delete(
            url_for("api.remove_oauth2_server_token", id=oauth2_server_token.id)
        )
        db.session.commit()

        check_api_response(response, status_code=204)
        assert not OAuth2ServerToken.query.all()
