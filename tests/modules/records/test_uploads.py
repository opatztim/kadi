# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from io import BytesIO

import pytest
from flask import current_app

import kadi.lib.constants as const
from kadi.lib.exceptions import KadiChecksumMismatchError
from kadi.lib.exceptions import KadiFilesizeExceededError
from kadi.lib.exceptions import KadiFilesizeMismatchError
from kadi.lib.security import hash_value
from kadi.lib.storage.local import LocalStorage
from kadi.modules.records.models import ChunkState
from kadi.modules.records.models import FileState
from kadi.modules.records.models import Upload
from kadi.modules.records.models import UploadState
from kadi.modules.records.models import UploadType
from kadi.modules.records.uploads import delete_upload
from kadi.modules.records.uploads import merge_chunk_data
from kadi.modules.records.uploads import remove_upload
from kadi.modules.records.uploads import save_chunk_data
from kadi.modules.records.uploads import save_upload_data


def test_delete_upload(dummy_upload):
    """Test if deleting uploads works correctly."""
    delete_upload(dummy_upload)
    assert dummy_upload.state == UploadState.INACTIVE


def test_remove_upload(new_upload):
    """Test if removing uploads works correctly."""
    chunk_data = 10 * b"x"
    upload = new_upload(size=len(chunk_data))

    save_chunk_data(upload, BytesIO(chunk_data), index=0, size=len(chunk_data))
    remove_upload(upload)

    assert not Upload.query.all()
    assert not os.listdir(upload.storage.root_directory)


def test_save_chunk_data_success(new_upload):
    """Test if saving valid chunk data works correctly."""
    chunk_data = 10 * b"x"
    checksum = hash_value(chunk_data, alg="md5")
    upload = new_upload(size=len(chunk_data))

    chunk = save_chunk_data(
        upload, BytesIO(chunk_data), index=0, size=len(chunk_data), checksum=checksum
    )

    assert chunk.state == ChunkState.ACTIVE
    assert chunk.checksum is not None
    assert upload.storage.exists(chunk.identifier)


@pytest.mark.parametrize(
    "chunk_size,checksum,exception",
    [
        (0, None, KadiFilesizeExceededError),
        (2, None, KadiFilesizeMismatchError),
        (1, "test", KadiChecksumMismatchError),
    ],
)
def test_save_chunk_data_error(chunk_size, checksum, exception, new_upload):
    """Test if saving invalid chunk data works correctly."""
    upload = new_upload(size=chunk_size)

    with pytest.raises(exception):
        save_chunk_data(
            upload,
            BytesIO(b"x"),
            index=0,
            size=chunk_size,
            checksum=checksum,
        )


def test_merge_chunk_data_success(dummy_record, new_upload):
    """Test if merging valid chunk data works correctly."""
    file_data = 10 * b"x"
    chunk_data = file_data[: len(file_data) // 2]
    prev_timestamp = dummy_record.last_modified
    description = "test"
    mimetype = "test/test"

    chunk_checksum = hash_value(chunk_data, alg="md5", hexadecimal=False)
    combined_checksum = hash_value(2 * chunk_checksum, alg="md5")
    combined_checksum = f"{combined_checksum}-2"

    upload = new_upload(
        size=len(file_data), description=description, mimetype=mimetype, chunk_count=2
    )

    for index in [0, 1]:
        save_chunk_data(upload, BytesIO(chunk_data), index=index, size=len(chunk_data))

    file = merge_chunk_data(upload)

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert not upload.storage.exists(upload.identifier)
    assert file.state == FileState.ACTIVE
    assert file.description == description
    assert file.size == len(file_data)
    assert file.checksum == combined_checksum
    assert file.mimetype == mimetype
    assert file.magic_mimetype == const.MIMETYPE_TEXT
    assert file.revisions.count() == 1
    assert file.storage.exists(file.identifier)

    # Replace the previous file.
    file_data = b'{"foo": [], "bar": "baz"}'
    prev_timestamp = dummy_record.last_modified
    prev_file_id = file.id

    file_checksum = hash_value(file_data, alg="md5", hexadecimal=False)
    combined_checksum = hash_value(file_checksum, alg="md5")
    combined_checksum = f"{combined_checksum}-1"

    upload = new_upload(file=file, size=len(file_data))
    save_chunk_data(upload, BytesIO(file_data), index=0, size=len(file_data))
    file = merge_chunk_data(upload)

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert not upload.storage.exists(upload.identifier)
    assert file.state == FileState.ACTIVE
    assert file.description == description
    assert file.size == len(file_data)
    assert file.checksum == combined_checksum
    assert file.mimetype == const.MIMETYPE_TEXT
    assert file.magic_mimetype == const.MIMETYPE_JSON
    assert file.revisions.count() == 2
    assert file.storage.exists(file.identifier)
    assert file.id == prev_file_id


def test_merge_chunk_data_error(dummy_file, new_upload):
    """Test if merging invalid chunk data works correctly."""
    file_data = b"x"

    upload = new_upload(size=2, file=dummy_file)
    save_chunk_data(upload, BytesIO(file_data), index=0, size=len(file_data))

    with pytest.raises(KadiFilesizeMismatchError):
        merge_chunk_data(upload)

    assert upload.state == UploadState.INACTIVE
    assert dummy_file.state == FileState.ACTIVE
    assert dummy_file.storage.exists(dummy_file.identifier)


def test_save_upload_data_success(monkeypatch, tmp_path, dummy_record, new_upload):
    """Test if saving valid upload data works correctly."""
    file_data = 10 * b"x"
    prev_timestamp = dummy_record.last_modified
    description = "test"
    mimetype = "test/test"
    checksum = hash_value(file_data, alg="md5")

    upload = new_upload(
        size=len(file_data),
        description=description,
        mimetype=mimetype,
        upload_type=UploadType.DIRECT,
    )
    file = save_upload_data(upload, BytesIO(file_data), checksum=checksum)

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert not upload.storage.exists(upload.identifier)
    assert file.state == FileState.ACTIVE
    assert file.description == description
    assert file.size == len(file_data)
    assert file.checksum == checksum
    assert file.mimetype == mimetype
    assert file.magic_mimetype == const.MIMETYPE_TEXT
    assert file.revisions.count() == 1
    assert file.storage.exists(file.identifier)

    # Replace the previous file.
    file_data = b'{"foo": [], "bar": "baz"}'
    prev_timestamp = dummy_record.last_modified
    prev_file_id = file.id
    checksum = hash_value(file_data, alg="md5")

    upload = new_upload(file=file, size=len(file_data), upload_type=UploadType.DIRECT)
    file = save_upload_data(upload, BytesIO(file_data), checksum=checksum)

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert not upload.storage.exists(upload.identifier)
    assert file.state == FileState.ACTIVE
    assert file.description == description
    assert file.size == len(file_data)
    assert file.checksum == checksum
    assert file.mimetype == const.MIMETYPE_TEXT
    assert file.magic_mimetype == const.MIMETYPE_JSON
    assert file.revisions.count() == 2
    assert file.storage.exists(file.identifier)
    assert file.id == prev_file_id

    # Replace the previous file using a different storage type.
    dummy_storage_type = "dummy"
    dummy_storage_path = os.path.join(tmp_path, dummy_storage_type)
    os.makedirs(dummy_storage_path)

    dummy_storage = LocalStorage(dummy_storage_path)
    dummy_storage._storage_type = dummy_storage_type
    local_storage = current_app.config["STORAGE_PROVIDERS"][const.STORAGE_TYPE_LOCAL]

    storage_providers = {
        const.STORAGE_TYPE_LOCAL: local_storage,
        dummy_storage_type: dummy_storage,
    }
    monkeypatch.setitem(current_app.config, "STORAGE_PROVIDERS", storage_providers)

    upload = new_upload(
        file=file,
        size=len(file_data),
        upload_type=UploadType.DIRECT,
        storage_type=dummy_storage_type,
    )
    file = save_upload_data(upload, BytesIO(file_data))

    assert file.storage_type == dummy_storage_type
    assert file.storage.exists(file.identifier)
    assert not os.listdir(local_storage.root_directory)


@pytest.mark.parametrize(
    "size,checksum,exception",
    [
        (0, None, KadiFilesizeExceededError),
        (2, None, KadiFilesizeMismatchError),
        (1, "test", KadiChecksumMismatchError),
    ],
)
def test_save_upload_data_error(size, checksum, exception, dummy_file, new_upload):
    """Test if saving invalid upload data works correctly."""
    upload = new_upload(size=size, file=dummy_file)

    with pytest.raises(exception):
        save_upload_data(upload, BytesIO(b"x"), checksum=checksum)

    assert upload.state == UploadState.INACTIVE
    assert dummy_file.state == FileState.ACTIVE
    assert dummy_file.storage.exists(dummy_file.identifier)
