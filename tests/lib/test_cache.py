# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.lib.cache import memoize_request


def test_memoize_request(monkeypatch, request_context):
    """Test if the "memoize_request" decorator works correctly."""
    monkeypatch.setitem(current_app.config, "TESTING", False)

    function_calls = 0

    @memoize_request
    def _cached_func(a, b=1, **kwargs):
        nonlocal function_calls
        function_calls += 1

    _cached_func(1)
    _cached_func(1, 1)
    _cached_func(1, b=1)
    _cached_func(a=1, b=1)

    assert function_calls == 1

    _cached_func(1, 2)
    _cached_func(1, b=2)
    _cached_func(a=1, b=2)

    assert function_calls == 2

    _cached_func(1, 1, c=3)
    _cached_func(1, b=1, c=3)
    _cached_func(a=1, b=1, c=3)

    assert function_calls == 3
